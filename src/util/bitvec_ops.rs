use bit_vec::BitVec;
use crate::util::bit_math;



/// Generic trait.
/// Provides bidirectional transformation between type and `BitVec`
pub trait BitVecOps {
    /// Transforms `self` into `BitVec` of size `max_type_bit_size()`
    fn into_bit_vec(self) -> BitVec;
    /// Reads `self` from `BitVec` of size `max_type_bit_size()`
    fn from_bit_vec(bv: BitVec) -> Self;
    /// Returns minimal number of bits that's enough
    /// to store *any* value of this type.
    fn max_type_bit_size() -> u8;
    /// Returns `usize` for `self`
    fn _cast_usize(&self) -> usize;
    /// Returns minimal number of bits that's enough
    /// to store *only this* value (`self`)
    fn trim_bit_size(&self) -> u8 {
        // Casting self to `usize` and calling `bit_places`
        bit_math::bit_places(self._cast_usize())
    }
}

/// Transforming `BitVec` to boxed slice of bytes `Box<[u8]>`
fn bitvec_to_bytes(vec: BitVec) -> Box<[u8]> {
    vec.to_bytes().into_boxed_slice()
}

/// Char implementation is based on implementation for `u32`
impl BitVecOps for char {
    fn into_bit_vec(self) -> BitVec {
        (self as u32).into_bit_vec()
    }

    fn from_bit_vec(bv: BitVec<u32>) -> Self {
        std::char::from_u32(u32::from_bit_vec(bv)).unwrap()
    }

    fn max_type_bit_size() -> u8 {
        u32::max_type_bit_size()
    }

    fn _cast_usize(&self) -> usize {
        *self as usize
    }
}

impl BitVecOps for u32 {
    fn into_bit_vec(self) -> BitVec {
        BitVec::from_bytes(&self.to_be_bytes())
    }

    fn from_bit_vec(bv: BitVec<u32>) -> Self {
        let bytes = bitvec_to_bytes(bv);

        ((bytes[0] as u32) << 24) +
            ((bytes[1] as u32) << 16) +
            ((bytes[2] as u32) << 8) +
            ((bytes[3] as u32) << 0)
    }

    fn max_type_bit_size() -> u8 {
        4 * 8
    }

    fn _cast_usize(&self) -> usize {
        *self as usize
    }
}

impl BitVecOps for u16 {
    fn into_bit_vec(self) -> BitVec {
        BitVec::from_bytes(&self.to_be_bytes())
    }

    fn from_bit_vec(bv: BitVec<u32>) -> Self {
        let bytes = bitvec_to_bytes(bv);

        ((bytes[2] as u16) << 8) +
            ((bytes[3] as u16) << 0)
    }

    fn max_type_bit_size() -> u8 {
        2 * 8
    }

    fn _cast_usize(&self) -> usize {
        *self as usize
    }
}

impl BitVecOps for u8 {
    fn into_bit_vec(self) -> BitVec {
        BitVec::from_bytes(&self.to_be_bytes())
    }

    fn from_bit_vec(bv: BitVec<u32>) -> Self {
        let bytes = bitvec_to_bytes(bv);
        bytes[0]
    }

    fn max_type_bit_size() -> u8 {
        8
    }

    fn _cast_usize(&self) -> usize {
        *self as usize
    }
}

