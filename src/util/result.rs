use std::fmt::{Display, Formatter};
use std::io::Error;

pub type WResult<T> = std::result::Result<T, WError>;

pub enum WError {
    Generic(&'static str),
    IO(std::io::Error)
}

impl Display for WError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            WError::Generic(str) => f.write_str(str),
            WError::IO(io_error) => std::fmt::Display::fmt(&io_error, f)
        }
    }
}

impl From<std::io::Error> for WError {
    fn from(io_error: Error) -> Self {
        WError::IO(io_error)
    }
}
