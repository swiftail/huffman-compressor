use std::collections::HashMap;
use std::fmt::{Debug, Formatter};
use std::fmt;
use std::hash::Hash;

/// Bidirectional map of `<L,R>`.
/// Based on two internal `HashMap`s for each direction.
pub struct BiMap<L, R> {
    right_by_left: HashMap<L, R>,
    left_by_right: HashMap<R, L>,
}

impl<L, R> BiMap<L, R>
    where L: Hash + Eq + Clone,
          R: Hash + Eq + Clone {

    /// Simple constructor
    pub fn new() -> BiMap<L, R> {
        BiMap {
            right_by_left: HashMap::new(),
            left_by_right: HashMap::new(),
        }
    }

    /// Gets `R` by `L`
    pub fn get_right(&self, key: &L) -> Option<&R> {
        self.right_by_left.get(key)
    }

    /// Gets `L` by `R`
    pub fn get_left(&self, key: &R) -> Option<&L> {
        self.left_by_right.get(key)
    }

    /// Checks if `R` exists for `L`
    pub fn contains_left(&self, key: &L) -> bool {
        self.right_by_left.contains_key(&key)
    }

    /// Checks if `L` exists for `R`
    pub fn contains_right(&self, key: &R) -> bool {
        self.left_by_right.contains_key(&key)
    }

    /// Inserts entry to both maps
    pub fn insert(&mut self, l: L, r: R) {
        self.left_by_right.insert(r.clone(), l.clone());
        self.right_by_left.insert(l, r);
    }

    pub fn len(&self) -> u32 {
        self.left_by_right.len() as u32
    }

    /// Returns pointer to internal `HashMap<L,R>`
    pub fn get_backing_right_by_left(&self) -> &HashMap<L,R> {
        &self.right_by_left
    }

    /// Returns pointer to internal `HashMap<R,L>`
    pub fn get_backing_left_by_right(&self) -> &HashMap<R,L> {
        &self.left_by_right
    }
}

/// Debug printing
impl<L, R> Debug for BiMap<L, R>
    where L: Hash + Eq + Clone + Debug,
          R: Hash + Eq + Clone + Debug {
    /// Calls `fmt` for inner `HashMap<R,L>`
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        self.left_by_right.fmt(f)
    }
}
