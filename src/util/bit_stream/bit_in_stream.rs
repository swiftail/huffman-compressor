use std::cmp::max;

use bit_vec::BitVec;

use crate::util::bit_math;
use crate::util::bitvec_ops::BitVecOps;
use crate::workload::shared::constant::BIT_0;

/// Input stream that supports bit reading and variable-length data types.
///
/// Reading comes from the data vector, that must be reversed
/// (since the implementation is using `pop()` method)
pub struct BitInStream {
    /// Source data byte array
    /// Must be reversed
    data: Vec<u8>,
    /// Current byte buffer
    buffer: u8,
    /// Current byte buffer position
    buffer_pos: u8,
    /// Is reading available
    available: bool,
}

/// Logic methods
impl BitInStream {
    /// Requires reading is available
    fn require_available(&self) {
        assert!(self.available)
    }
    /// Takes next byte from `data` vector,
    /// updates buffer and resets buffer position
    ///
    /// If there is no more bytes available, `self.available`
    /// is set to false
    fn peek_buffer(&mut self) {
        if self.data.is_empty() {
            self.available = false
        } else {
            self.buffer_pos = 0;
            self.buffer = self.data.pop().unwrap()
        }
    }
    /// Checks if current byte buffer is over
    /// Then peeks the buffer if it is
    fn check_overflow(&mut self) {
        if self.buffer_pos == 8 {
            self.peek_buffer()
        }
    }
    /// Getter method
    pub fn is_available(&self) -> bool {
        self.available
    }
}

/// Reading
impl BitInStream {
    /// Semantic implementation that fallbacks to `read_generic()`
    pub fn read_u8(&mut self) -> u8 {
        self.read_generic()
    }
    /// Semantic implementation that fallbacks to `read_generic()`
    pub fn read_u16(&mut self) -> u16 {
        self.read_generic()
    }
    /// Semantic implementation that fallbacks to `read_generic()`
    pub fn read_u32(&mut self) -> u32 {
        self.read_generic()
    }
    /// Semantic implementation that fallbacks to `read_generic()`
    pub fn read_char(&mut self) -> char {
        self.read_generic()
    }

    /// Reads generic value.
    /// Value here is of type `V` that implements `BitVecOps`.
    ///
    /// # Algorithm:
    /// 1. Read checked-size `BitVec` of `V::max_type_bit_size()` bits
    /// 2. Convert to `V` using `V::from_bit_vec()`
    ///
    /// This method utilizes `read_vec_checked`, so resulting vector's size
    /// is the max specified size of type `V`.
    fn read_generic<V>(&mut self) -> V
        where V: BitVecOps {
        V::from_bit_vec(self.read_vec_checked(V::max_type_bit_size()))
    }

    /// Reads generic value of max size `len`.
    /// Value here is of type `V` that implements `BitVecOps`.
    ///
    /// # Algorithm:
    /// 1. Create an empty `BitVec`
    /// 2. Fill (`V::max_type_bit_size() - len`) bits with `BIT_0`.
    ///    These bits are not used, but we need to pad the vector
    ///    to size `V::max_type_bit_size()` in order to restore it
    ///    using `V::from_bit_vec()` later.
    /// 3. Read checked-size (`len`) `BitVec` of bits and append
    ///    this vector to one we've created before
    /// 4. Convert to `V` using `V::from_bit_vec()`
    pub fn read_var<V>(&mut self, len: u8) -> V
        where V: BitVecOps {
        let mut vec = BitVec::new();
        for _ in 0..(V::max_type_bit_size() - len) {
            vec.push(BIT_0);
        }
        vec.append(&mut self.read_vec_checked(len));
        V::from_bit_vec(vec)
    }

    /// Reads `BitVec` of size `len`.
    /// Resulting `BitVec` is *always* of size `len`.
    pub fn read_vec_checked(&mut self, len: u8) -> BitVec {
        let mut vec = BitVec::new();
        for _ in 0..len {
            vec.push(self.read_bit());
        }
        vec
    }

    /// Reads single bit from `buffer`.
    pub fn read_bit(&mut self) -> bool {
        self.require_available();

        // Bit operation to extract bit that is set on `buffer_pos` position in `buffer`
        let bit = bit_math::get_n_bit(&self.buffer, self.buffer_pos);

        // Buffer pos is incremented
        self.buffer_pos += 1;
        // Checking for overflow
        self.check_overflow();

        // Returning result
        bit
    }

    /// Reads single byte from `buffer`
    ///
    /// This method doesn't utilize `read_bit()`, it reads bytes directly
    /// from the buffer.
    ///
    /// Thus, the buffer required to not be dirty.
    /// This methods panics if called when buffer is dirty.
    fn read_byte(&mut self) -> u8 {
        self.require_available();

        assert_eq!(self.buffer_pos, 0, "read_byte() called when buffer is dirty");

        let result = self.buffer;
        self.peek_buffer();
        result
    }
}

impl BitInStream {
    /// Locates byte sequence in the data stream
    ///
    /// If the sequence was found:
    /// * returns `true`
    /// * next buffer byte is the byte after the end of the sequence
    ///
    /// If the sequence was not found:
    /// * returns `false`
    /// * this stream becomes unavailable
    pub fn locate(&mut self, bytes: Vec<u8>) -> bool {

        let mut index = 0;
        let brd = bytes.len();

        while self.available && index < brd {
            let cmp = self.read_byte();
            if cmp == *bytes.get(index).unwrap() {
                index += 1;
            } else {
                index = 0;
            }
        }

        index == brd
    }
}

/// Constructor function
impl BitInStream {
    /// Creates new `BitInStream` from
    /// byte slice `data`.
    pub fn new(data: &[u8]) -> BitInStream {
        let mut vec = Vec::from(data);
        // `BitInStream` requires us to reverse original data.
        vec.reverse();
        let mut stream = BitInStream {
            data: vec,
            buffer: 0,
            buffer_pos: 0,
            available: true,
        };
        // `peek_buffer` is used to apply the first byte.
        stream.peek_buffer();
        stream
    }
}
