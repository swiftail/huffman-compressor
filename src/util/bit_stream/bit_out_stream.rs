

use bit_vec::{BitVec};

use crate::util::bit_math;
use crate::util::bitvec_ops::BitVecOps;

/// Output stream that supports bit writing and variable-length data types.
///
/// This struct writes to `Vec<u8>`.
pub struct BitOutStream {
    /// Byte vector of already finished bytes
    completed: Vec<u8>,
    /// Current write buffer
    buffer: u8,
    /// Current write buffer pos
    buffer_pos: u8,
}

/// Logic
impl BitOutStream {
    /// Pushes current `buffer` to `completed`,
    /// resets `buffer` and `buffer_pos`.
    fn push_buffer(&mut self) {
        self.completed.push(self.buffer);
        self.buffer_pos = 0;
        self.buffer = 0;
    }
    /// Checks for `buffer_pos` overflow,
    /// calls `push_buffer` if overflow is detected.
    fn check_overflow(&mut self) {
        if self.buffer_pos == 8 {
            self.push_buffer()
        }
    }
    /// Transforms `BitOutStream` to boxed byte slice `Box<[u8]>`.
    ///
    /// If buffer is not finished, calls `push_buffer`.
    pub fn finish(mut self) -> Box<[u8]> {
        if self.buffer_pos != 0 {
            self.push_buffer()
        }
        self.completed.into_boxed_slice()
    }
    /// Returns current in bits
    pub fn raw_bit_len(&self) -> usize {
        self.completed.len() * 8 + self.buffer_pos as usize
    }
}

/// Write methods
impl BitOutStream {
    /// Semantic implementation that fallbacks to `write_generic()`
    pub fn write_u8(&mut self, val: u8) {
        self.write_generic(val)
    }
    /// Semantic implementation that fallbacks to `write_generic()`
    pub fn write_u16(&mut self, val: u16) {
        self.write_generic(val)
    }
    /// Semantic implementation that fallbacks to `write_generic()`
    pub fn write_u32(&mut self, val: u32) {
        self.write_generic(val)
    }
    /// Semantic implementation that fallbacks to `write_generic()`
    pub fn write_char(&mut self, val: char) {
        self.write_generic(val)
    }

    /// Writes generic value.
    /// `val` here is of type `V` that implements `BitVecOps`.
    ///
    /// # Algorithm:
    /// 1. Transform `val` into bit vec (resulting `BitVec` of size `V::max_type_bit_size()`)
    /// 2. Write this `BitVec` using `write_vec_unchecked`
    fn write_generic<V>(&mut self, val: V)
        where V: BitVecOps {
        self.write_vec_unchecked(val.into_bit_vec())
    }

    /// Writes generic value of variable bit length.
    /// `val` here is of type `V` that implements `BitVecOps`.
    ///
    /// # Algorithm:
    /// 1. Get bit size of `val`
    /// 2. Assert bit size is not greater than `len`
    /// 3. Transform `val` to `BitVec` of length `V::max_type_bit_size()`
    /// 4. Write last `len` bits of `BitVec`
    pub fn write_var<V>(&mut self, len: u8, val: V)
        where V: BitVecOps {
        // Get min number of bits to store this value
        let trim_bit_size = val.trim_bit_size();

        // Require that size to store this value
        // is not greater than supplied `len`
        assert!(len >= trim_bit_size);

        // Transform value into `BitVec` of max size
        let bv = val.into_bit_vec();

        // How many first bits of `bv` should we skip
        let k = bv.len() - len as usize;

        for i in 0..len {
            // Getting bit `k+i`
            let bit = bv.get(k + i as usize).unwrap();
            // Writing to vector
            self.write_bit(bit);
        }
    }

    /// Writes all bits of `BitVec`.
    pub fn write_vec_unchecked(&mut self, bits: BitVec) {
        bits.iter().for_each(|b| self.write_bit(b));
    }

    /// Writes single bit to `buffer`
    ///
    /// This is the only method that actually writes data to buffer,
    /// all other `write_*` methods are based on this one.
    ///
    /// This is the only place where `check_overflow()` is called
    fn write_bit(&mut self, bit: bool) {
        // If bit is `BIT_1`, we should write it.
        // No need to write `BIT_0` since buffer is already filled with zeros.
        if bit {
            // Setting `buffer` bit at `buffer_pos` to `1`
            bit_math::set_n_bit_1(&mut self.buffer, self.buffer_pos)
        }
        // Moving buffer_pos
        self.buffer_pos += 1;
        // Checking for available overflow
        self.check_overflow()
    }

    /// Writes single byte directly to `data`, avoiding `buffer`.
    ///
    /// This function requires the buffer to be clean (`buffer_pos` must be `0`).
    ///
    /// If the buffer is dirty, this function panics.
    pub fn write_byte_clean(&mut self, byte: u8) {
        assert_eq!(self.buffer_pos, 0, "write_byte_clean() called but buffer is dirty");
        self.buffer = byte;
        self.push_buffer()
    }

    /// Writes `Vec<u8>` directly to `data`, avoiding `buffer`.
    pub fn write_byte_vec_clean(&mut self, vec: Vec<u8>) {
        for byte in vec {
            self.write_byte_clean(byte)
        }
    }
}

/// Simple constructor function
impl BitOutStream {
    pub fn new() -> BitOutStream {
        BitOutStream {
            completed: vec![],
            buffer: 0,
            buffer_pos: 0,
        }
    }
}
