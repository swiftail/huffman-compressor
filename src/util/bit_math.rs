/// Bit math utilities

/// Returns minimal number of bits to store the `num`
pub fn bit_places(num: usize) -> u8 {
    ((num as f32).log2() + 1.) as u8
}

pub fn set_n_bit_1(buffer: &mut u8, pos: u8) {
    *buffer |= 1 << pos;
}

pub fn get_n_bit(buffer: &u8, pos: u8) -> bool {
    (buffer >> pos) & 1 == 1
}
