use std::time::Instant;

use ansi_term::Colour::*;

use crate::cli::app::build_app;
use crate::cli::config::config::Config;
use crate::cli::config::mode::Mode;
use crate::cli::runner::Runner;
use crate::util::result::{WResult};


fn process_silent(config: &Config, runner: Runner) -> WResult<()> {
    let input_data = config.input.read()?;
    let output_data = runner.run(input_data)?;
    config.output.write(output_data)?;
    Ok(())
}

fn process_stats(config: &Config, runner: Runner) -> WResult<()> {

    if let Err(_e) = ansi_term::enable_ansi_support() {
        println!("error: Failed to enable ansi support")
    }

    println!("{}", Cyan.bold().paint("[[[ POC huffman-processor v0.1.0 with verbose output ]]]"));
    println!("{} Reading input data from {:?}...", Green.paint("[*]"), config.input);
    let input_data = config.input.read()?;
    let input_data_len = input_data.len() as f32;
    println!("{} Input data size: {} Bytes", Blue.paint("(i)"), input_data_len);
    println!("{} Processing data...", Green.paint("[*]"));

    let start = Instant::now();
    let output_data = runner.run(input_data)?;
    let output_data_len = output_data.len() as f32;
    let time = start.elapsed().as_secs_f32();
    println!("{} Done processing data in {}s. ({} Bytes/Sec)", Blue.paint("(i)"), time, input_data_len / time);
    println!("{} Output data size: {} Bytes", Blue.paint("(i)"), output_data.len());

    if config.mode == Mode::Compress {
        let enh = 100. - (output_data_len / input_data_len * 100.);
        let color = match enh {
            _ if enh < 0. => Red.bold(),
            _ if enh  < 25. => Yellow.bold(),
            _ => Green.bold()
        };
        let formatted = format!("(i) Compression enhancement: {:.2}%", enh);
        println!("{}", color.paint(formatted));
    }

    println!("{} Writing data to {:?}...", Green.paint("[*]"), config.output);
    config.output.write(output_data)?;
    println!("{} Done.", Green.paint("[*]"));
    Ok(())
}

pub fn launch() {
    let config = Config::resolve(build_app());
    let runner = Runner::new(&config.mode);

    let result = if config.verbose {
        process_stats(&config, runner)
    } else {
        process_silent(&config, runner)
    };

    if let Err(e) = result {
        eprintln!("{} {}", Red.bold().paint("Error:"), e);
        std::process::exit(2);
    }
}
