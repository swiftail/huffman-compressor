use clap::{App, ArgGroup, Arg};

pub fn build_app<'a>() -> App<'a> {
    App::new("Huffman Processor")
        .version("0.1.0")
        .arg("--decompress                'Enables decompress mode'")
        .arg("--compress                  'Enables compress mode'")
        .arg("--output-path=[PATH]        'Sets output file'")
        .arg("-o, --output-name=[NAME]    'Sets output name. File will be saved in current working directory with extension .hf0'")
        .arg("-i, --input-path=[PATH]     'Sets input file'")
        .arg("--verbose                   'Turns verbose mode on'")
        .arg("--overwrite                 'Overwrite contents of output file'")
        // Stdin argument
        .arg(Arg::with_name("stdin")
            .long("--stdin")
            .about("Sets input to stdin")
            // Binary data from stdin is not supported
            .conflicts_with("decompress"))
        .arg(Arg::with_name("stdout")
            .long("--stdout")
            .about("Sets output to stdout")
            // Binary data to stdout is not supported
            .conflicts_with("compress")
            .conflicts_with("overwrite"))
        // Mode group: --compress or --decompress
        .group(ArgGroup::with_name("mode")
            .args(&["compress", "decompress"])
            .required(true))
        // Output group: --output-file or --stdout
        .group(ArgGroup::with_name("output")
            .args(&["output-path", "stdout", "output-name"])
            .required(true))
        // Input group: --input-file or --stdin
        .group(ArgGroup::with_name("input")
            .args(&["input-path", "stdin"])
            .required(true))
}
