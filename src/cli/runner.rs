use crate::cli::config::mode::Mode;
use crate::util::result::WResult;
use crate::workload::interface;

pub enum Runner {
    Compressor,
    Decompressor
}

impl Runner {
    pub fn new(mode: &Mode) -> Runner {
        match mode {
            Mode::Compress => Runner::Compressor,
            Mode::Decompress => Runner::Decompressor,
        }
    }

    pub fn run(&self, data: Box<[u8]>) -> WResult<Box<[u8]>> {
         match self {
             Runner::Compressor => interface::compress(data.as_ref()),
             Runner::Decompressor => interface::decompress(data.as_ref()),
         }
    }
}
