use std::path::{PathBuf, Path};

use clap::App;

use crate::cli::config::io;
use crate::cli::config::mode::Mode;


pub struct Config {
    pub mode: Mode,
    pub verbose: bool,
    pub input: io::Input,
    pub output: io::Output,
}

impl Config {
    pub fn resolve(app: App) -> Config {
        let r = app.get_matches();

        let mode: Mode = if r.is_present("compress") {
            Mode::Compress
        } else {
            Mode::Decompress
        };

        let input: io::Input = match r.value_of("input-path") {
            Some(path) => io::Input::File { path: PathBuf::from(path) },
            None => io::Input::Stdin
        };

        let output: io::Output = if let Some(path) = r.value_of("output-path") {
            io::Output::File {
                path: PathBuf::from(path),
                overwrite: r.is_present("overwrite")
            }
        } else if let Some(name) = r.value_of("output-name") {
            io::Output::File {
                path: PathBuf::from(format!("./{}.hf0", name)),
                overwrite: r.is_present("overwrite")
            }
        } else {
            io::Output::Stdout
        };

        let verbose: bool = r.is_present("verbose");

        Config {
            mode,
            verbose,
            input,
            output,
        }
    }
}
