#[derive(PartialEq)]
pub enum Mode {
    Compress,
    Decompress
}
