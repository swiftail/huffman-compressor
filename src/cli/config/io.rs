
use std::io::{Read, Write};
use std::path::{PathBuf};
use crate::util::result::{WResult, WError};

#[derive(Debug)]
pub enum Input {
    Stdin,
    File {
        path: PathBuf
    },
}

impl Input {
    pub fn read(&self) -> WResult<Box<[u8]>> {
        match self {
            Input::Stdin => {
                let mut buf = Vec::new();
                std::io::stdin().read_to_end(&mut buf)?;
                Ok(buf.into_boxed_slice())
            }
            Input::File { path } => {
                let mut buf = Vec::new();
                std::fs::File::open(path)?.read_to_end(&mut buf)?;
                Ok(buf.into_boxed_slice())
            }
        }
    }
}

#[derive(Debug)]
pub enum Output {
    Stdout,
    File {
        path: PathBuf,
        overwrite: bool,
    },
}

impl Output {
    pub fn write(&self, data: Box<[u8]>) -> WResult<()> {
        match self {
            Output::Stdout => {
                std::io::stdout().write_all(data.as_ref())?;
                std::io::stdout().flush();
                Ok(())
            }
            Output::File {
                path,
                overwrite
            } => {
                if path.exists() && !overwrite {
                    return Err(WError::Generic("File already exists and no --overwrite option supplied"))
                }
                let mut file = std::fs::File::create(path)?;
                file.write_all(data.as_ref())?;
                file.flush();
                Ok(())
            }
        }
    }
}
