use crate::workload::compress::alg::*;
use crate::util::result::WResult;

/// Public interface for this module.

/// Compresses byte slice
pub fn compress(data: &[u8]) -> WResult<Box<[u8]>> {
    // First, we're building frequency map for the bytes
    let freq_map = frequency_map::build_frequency_map(data);
    // Then we're building the huffman tree from this frequency map
    let huffman_tree = tree_builder::build_huffman_tree(freq_map);
    // Then building code table for this tree
    let code_table = code_table_builder::build_code_table(&huffman_tree);

    // Finally, encoding data using `code_table`
    encoder::encode(code_table, data)
}
