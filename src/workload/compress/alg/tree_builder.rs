use std::collections::{BinaryHeap, HashMap};

use crate::workload::shared::node::Node;
use crate::workload::shared::constant::{NULL_MARKER};

/// This method builds huffman binary tree for given frequency map.
///
/// Returns the root node of the tree.
pub fn build_huffman_tree(freq_map: HashMap<u8, u32>) -> Node {
    // Creating `BinaryHeap<Node>` (e.g. a priority queue)
    let mut heap = BinaryHeap::<Node>::new();

    // Filling the heap with all the nodes from the frequency map
    for (c, freq) in freq_map {
        heap.push(Node::new_leaf(c, freq))
    }

    // Compressing the heap into one root node
    while heap.len() != 1 {
        let left = heap.pop().unwrap();
        let right = heap.pop().unwrap();

        let sum = left.get_freq() + right.get_freq();

        heap.push(Node::new(NULL_MARKER, sum, Box::new(left), Box::new(right)))
    }

    // The zero node is the root node.
    heap.into_sorted_vec().remove(0)
}
