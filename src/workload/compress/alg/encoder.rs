

use crate::util::bit_math;
use crate::util::bit_stream::bit_out_stream::BitOutStream;

use crate::workload::shared::code_table::CodeTable;

use crate::workload::shared::constant::HEADER;
use crate::util::result::WResult;

/// Writes `HEADER` sequence
fn write_header(stream: &mut BitOutStream) {
    stream.write_byte_vec_clean(HEADER.to_vec())
}

/// Writes the `CodeTable` to `BitOutStream`
///
/// # Table format:
/// ```text
/// u32 : table len
/// u8  : code max
/// [
///     u8          : char
///     u[code max] : code len
///     u[code len] : code
/// ] * table len
/// ```
fn encode_table(stream: &mut BitOutStream, table: &CodeTable) {

    // Max number of bits to store code length
    let max_huffman_code_bit_length = bit_math::bit_places(table.get_max_bits() as usize);

    // Writing code table length
    stream.write_u32(table.len());
    // Writing max code bit length
    stream.write_u8(max_huffman_code_bit_length);

    // Iterating through the code table
    for (c, vec) in table.to_vec() {
        // Writing char [u8]
        stream.write_u8(c);
        // Writing var int of size `max_huffman_code_bit_length`
        // that stores code length
        stream.write_var(max_huffman_code_bit_length, vec.len() as u8);
        // Writing the code as a bitvec
        stream.write_vec_unchecked(vec);
    }
}

/// Writes the `data: &[u8]` to `BitOutStream`
fn encode_data(stream: &mut BitOutStream, table: &CodeTable, data: &[u8]) {
    // Writing length
    stream.write_u32(data.len() as u32);
    for x in data {
        // We can simply write every code as `BitVec`
        stream.write_vec_unchecked(table.encode(x))
    }
}

/// Encodes `data` using `CodeTable`
pub fn encode(table: CodeTable, data: &[u8]) -> WResult<Box<[u8]>> {
    // Creating new `BitOutStream`
    let mut bit_out_stream = BitOutStream::new();

    write_header(&mut bit_out_stream);
    // Writing table
    encode_table(&mut bit_out_stream, &table);
    // Writing data
    encode_data(&mut bit_out_stream, &table, data);

    // Finishing the stream and returning its data
    Ok(bit_out_stream.finish())
}
