use std::collections::HashMap;

/// Builds frequency map (`HashMap<u8,u32>`) for given byte array.
pub fn build_frequency_map(data: &[u8]) -> HashMap<u8, u32> {
    let mut map = HashMap::<u8, u32>::new();
    for c in data {
        *map.entry(*c).or_insert(0) += 1
    }
    map
}
