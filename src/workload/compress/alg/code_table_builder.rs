use bit_vec::BitVec;

use crate::workload::shared::code_table::{CodeTable, CodeTableBuffer};
use crate::workload::shared::constant::{BIT_0, BIT_1};
use crate::workload::shared::node::Node;

/// This recursive method fills `CodeTableBuffer` with
/// chars & their generated unique-prefixed huffman codes
fn iter_node(node: &Node, bit_buf: BitVec, buffer: &mut CodeTableBuffer) {

    // If left node is present, iterating it.
    for boxed_child_node in node.get_left() {
        // Copying the buffer
        let mut buf_cpy = bit_buf.clone();
        // And pushing 0 to the stack
        buf_cpy.push(BIT_0);
        // Iterating the child node
        iter_node(boxed_child_node, buf_cpy, buffer)
    }

    // If right node is present, iterating it.
    for boxed_child_node in node.get_right() {
        // Copying the buffer
        let mut buf_cpy = bit_buf.clone();
        // And pushing 1 to the stack
        buf_cpy.push(BIT_1);
        // Iterating the child node
        iter_node(boxed_child_node, buf_cpy, buffer)
    }

    // If this node is leaf, inserting data to buffer
    if node.is_leaf() {
        buffer.insert(node.get_data(), bit_buf)
    }
}

/// Builds `CodeTable` for huffman tree.
pub fn build_code_table(tree: &Node) -> CodeTable {
    // Creating buffer
    let mut buf = CodeTable::buffer();
    // Recursively filling it
    iter_node(tree, BitVec::new(), &mut buf);
    // Building the `CodeTable` from the buffer.
    buf.build()
}
