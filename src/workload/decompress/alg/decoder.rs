use bit_vec::BitVec;

use crate::util::bit_stream::bit_in_stream::BitInStream;
use crate::workload::shared::code_table::CodeTable;
use crate::workload::shared::constant::HEADER;
use crate::util::result::{WResult, WError};

/// Moves the stream buffer after the end of the `HEADER` sequence.
/// If no sequence was found, returns `Err`
fn locate_header(stream: &mut BitInStream) -> WResult<()> {
    if stream.locate(HEADER.to_vec()) {
        Ok(())
    } else {
        Err(WError::Generic("Failed to locate header in data stream"))
    }
}

/// Reads the `CodeTable` from `BitInStream`
///
/// # Table format:
/// ```text
/// u32 : table len
/// u8  : code max
/// [
///     u8          : char
///     u[code max] : code len
///     u[code len] : code
/// ] * table len
/// ```
fn decode_table(stream: &mut BitInStream) -> CodeTable {
    // Reading the table length
    let table_len = stream.read_u32();
    // Reading max huffman code bit length
    let max_huffman_code_bit_length = stream.read_u8();

    // Creating the code table buffer
    let mut buf = CodeTable::buffer();

    for _ in 0..table_len {
        // Reading the char
        let ch = stream.read_u8();
        // Reading the length of the code
        let code_bit_len = stream.read_var(max_huffman_code_bit_length);
        // Reading the code
        let code = stream.read_vec_checked(code_bit_len);

        // Inserting values to buffer
        buf.insert(ch, code);
    }

    // Building the buffer
    buf.build()
}

/// Reads the data from `BitInStream` using `CodeTable`
fn decode_data(stream: &mut BitInStream, table: &CodeTable) -> Box<[u8]> {
    // Length of input
    let length = stream.read_u32() as usize;
    // Buffer for decoded data
    let mut data = Vec::<u8>::new();
    // Buffer for unfinished code
    let mut buf_vec = BitVec::new();

    // Reading all the bits
    while stream.is_available() && data.len() < length {
        // Reading code bit by bit and inserting bits to the buffer
        let bit = stream.read_bit();
        buf_vec.push(bit);

        // If CodeTable is able to decode the buffer, decoding it,
        // pushing character to the data and dropping the buffer.
        if table.can_decode(&buf_vec) {
            data.push(table.decode(&buf_vec));
            buf_vec = BitVec::new();
        }
    }

    // Returning the data
    data.into_boxed_slice()
}

pub fn decode(data: &[u8]) -> WResult<Box<[u8]>> {
    // Creating new `BitInStream` from `data`
    let mut bit_in_stream = BitInStream::new(data);

    locate_header(&mut bit_in_stream)?;
    // Decoding the `CodeTable`
    let code_table = decode_table(&mut bit_in_stream);
    // Decoding the data
    let decoded_data = decode_data(&mut bit_in_stream, &code_table);

    Ok(decoded_data)
}
