use crate::workload::decompress::alg::decoder;
use crate::util::result::WResult;

/// Public interface for this module/

/// Decompresses byte slice
pub fn decompress(data: &[u8]) -> WResult<Box<[u8]>> {
    decoder::decode(data)
}
