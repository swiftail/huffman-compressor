use std::cmp::max;


use bit_vec::BitVec;

use crate::util::bi_map::BiMap;

/// Code table is data structure that stores
/// encoded pairs of characters and their huffman codes.
///
/// This struct is immutable and uses
/// `CodeTableBuffer` to access stored codes.
#[derive(Debug)]
pub struct CodeTable {
    /// Inner buffer
    buffer: CodeTableBuffer
}

impl CodeTable {
    /// Gets huffman code (`BitVec`) for character
    pub fn encode(&self, ch: &u8) -> BitVec {
        (*self.buffer.data.get_right(ch).unwrap()).clone()
    }
    /// Determines if huffman code exists
    pub fn can_decode(&self, code: &BitVec) -> bool {
        return self.buffer.data.contains_right(code);
    }
    /// Gets character for huffman code (`BitVec`)
    pub fn decode(&self, bits: &BitVec) -> u8 {
        *self.buffer.data.get_left(bits).unwrap()
    }
    /// Creates new `CodeTableBuffer`
    /// (This is not an instance method)
    pub fn buffer() -> CodeTableBuffer {
        return CodeTableBuffer { data: BiMap::new(), max_bits: 0 };
    }
    /// Returns `max_bits` of `buffer`
    pub fn get_max_bits(&self) -> u8 {
        self.buffer.max_bits
    }
    /// Returns `buffer` length
    pub fn len(&self) -> u32 {
        self.buffer.len()
    }
    /// Copies `buffer` data to a `Vec<(u8,BitVec)>`
    pub fn to_vec(&self) -> Vec<(u8, BitVec)> {
        let mut result = Vec::new();
        let l2r = self.buffer.data.get_backing_right_by_left();
        for (c, vec) in l2r {
            result.push((c.clone(), (*vec).clone()))
        }
        result
    }
}

/// Code table buffer is mutable
/// and actually stores all the data using `BiMap`
#[derive(Debug)]
pub struct CodeTableBuffer {
    /// Main storage
    data: BiMap<u8, BitVec>,
    /// Max bit len of huffman code
    max_bits: u8,
}

impl CodeTableBuffer {
    /// Returns length of storage
    fn len(&self) -> u32 {
        self.data.len()
    }
    /// Inserts new entry to the storage
    pub fn insert(&mut self, c: u8, bits: BitVec) {
        self.max_bits = max(self.max_bits, bits.len() as u8);
        self.data.insert(c, bits);
    }
    /// Transforms this buffer into `CodeTable`
    pub fn build(self) -> CodeTable {
        CodeTable { buffer: self }
    }
}

