use std::cmp::Ordering;

/// Huffman tree node
#[derive(PartialEq,Ord,Eq,Debug)]
pub struct Node {
    /// Character for this node
    ch: u8,
    /// Frequency of this character
    freq: u32,
    /// Left child node
    left: Option<Box<Node>>,
    /// Right child node
    right: Option<Box<Node>>,
}

/// Ordering implementation
impl PartialOrd for Node {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        // Using freq.cmp, but reversing it
        Some(self.freq.cmp(&other.freq).reverse())
    }
}

/// Constructors
impl Node {
    /// Creates a new `Node` without children
    pub fn new_leaf(ch: u8, freq: u32) -> Node {
        Node {
            ch,
            freq,
            left: None,
            right: None,
        }
    }
    /// Creates a new `Node` with children (`left` & `right`)
    pub fn new(ch: u8, freq: u32, left: Box<Node>, right: Box<Node>) -> Node {
        Node {
            ch,
            freq,
            left: Some(left),
            right: Some(right),
        }
    }
}

/// Accessors
impl Node {
    pub fn get_data(&self) -> u8 {
        self.ch
    }
    pub fn get_freq(&self) -> u32 {
        self.freq
    }
    pub fn get_left(&self) -> &Option<Box<Node>> {
        &self.left
    }
    pub fn get_right(&self) -> &Option<Box<Node>> {
        &self.right
    }
    pub fn is_leaf(&self) -> bool {
        self.right.is_none() && self.left.is_none()
    }
}
