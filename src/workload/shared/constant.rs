// Common constants

/// Zero byte
pub static NULL_MARKER: u8 = 0;
/// 0 bit in bool
pub static BIT_0: bool = false;
/// 1 bit in bool
pub static BIT_1: bool = true;
/// Data header
pub static HEADER: [u8; 4] = [0x50u8, 0x89u8, 0x0du8, 0x0au8];
