use crate::workload::{compress, decompress};
use crate::util::result::WResult;

/// Shared interface for `compress` and `decompress` functions.

/// Compresses `data`
pub fn compress(data: &[u8]) -> WResult<Box<[u8]>> {
    compress::interface::compress(data)
}

/// Decompresses `data`
pub fn decompress(data: &[u8]) -> WResult<Box<[u8]>> {
    decompress::interface::decompress(data)
}
