# About
Proof of concept binary data compressor & decompressor.
Algorithm is based on huffman coding with custom binary serialization of the code table.
This package contains implementation of the algorithm and the CLI wrapper.
# Building
```
cargo build --release
```
Executable files will be located in `target/release` directory.
# Compress (Windows example)
```
# This program will read all the text from stdin until the EOF.
huffman-compressor --verbose  --compress --stdin --output-path compressed.hf0 --overwrite 
```
# Decompress (Windows example)
```
huffman-compressor --verbose --decompress -i compressed.hf0 --stdout 
```
